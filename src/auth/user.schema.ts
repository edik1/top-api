import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
    timestamps: true,
    versionKey: false,
    toJSON: {
        virtuals: true,
    },
})
export class User extends Document {

    @Prop({
        type: String,
        unique: true,
    })
    email: string;

    @Prop({
        type: String,
    })
    passwordHash: string;
}

const UserSchema = SchemaFactory.createForClass(User);

UserSchema.virtual('id').get(function (this: User) {
    return this._id;
});

export { UserSchema as UserSchema };
