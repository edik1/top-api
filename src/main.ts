import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AuthModule } from './auth/auth.module';
import { ProductModule } from './product/product.module';
import { ReviewModule } from './review/review.module';
import { TopPageModule } from './top-page/top-page.module';

async function bootstrap() {

  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('api');

  const config = new DocumentBuilder()
    .setTitle('Top-Api')
    .setDescription('Top_api api v-1 description')
    .setVersion('1.0.0')
    .build();

  const document = SwaggerModule.createDocument(app, config, {
    include: [
      AuthModule,
      ProductModule,
      ReviewModule,
      TopPageModule

    ],
  });
  SwaggerModule.setup('swagger', app, document);



  await app.listen(5000, () => {
    console.log(`server start on http://localhost:5000`)
  });
}
bootstrap();