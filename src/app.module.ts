import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { TopPageModule } from './top-page/top-page.module';
import { ProductModule } from './product/product.module';
import { ReviewModule } from './review/review.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { TopPageService } from './top-page/top-page.service';


@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env']
    }),
    MongooseModule.forRoot('mongodb://localhost:27017', {
      // useNewUrlParser: true,
      // useUnifiedTopology: true,
      // connectTimeoutMS: 1000
    }),
    AuthModule,
    TopPageModule,
    ProductModule,
    ReviewModule
  ],
  providers: [],
})
export class AppModule { }