import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ReviewController } from './review.controller';
import { ReviewSchema } from './review.schema';
import { ReviewService } from './review.service';

@Module({
  controllers: [ReviewController],
  imports: [
    MongooseModule.forFeature([
      { name: 'Review', schema: ReviewSchema, collection: 'reviews' }
    ])
  ],
  providers: [ReviewService]
})
export class ReviewModule { }
