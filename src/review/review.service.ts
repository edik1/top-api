import { Injectable } from '@nestjs/common';
import { ModelType, DocumentType } from '@typegoose/typegoose/lib/types';
import { InjectModel } from 'nestjs-typegoose';
import { CreateReviewDto } from './dto/create-review.dto';
import { Review } from './review.schema';

class Leak {

}

const leaks = [];

@Injectable()
export class ReviewService {
    constructor(@InjectModel(Review) private readonly Review: ModelType<Review>) { }

    async create(dto: CreateReviewDto): Promise<DocumentType<Review>> {
        console.log(dto)
        return this.Review.create(dto);
    }

    async delete(id: string): Promise<DocumentType<Review> | null> {
        return this.Review.findByIdAndDelete(id).exec();
    }

    async findByProductId(productId: string): Promise<DocumentType<Review>[]> {
        return this.Review.find({ productId: productId }).exec();
    }

    async deleteByProductId(productId: string) {
        leaks.push(new Leak());
        return this.Review.deleteMany({ productId: productId }).exec();
    }
}
