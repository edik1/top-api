import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypeOptions } from 'mongoose';


export enum TopLevelCategory {
    Courses,
    Services,
    Books,
    Products
}

export class HhData {
    @Prop({
        type: Number
    })
    count: number;

    @Prop({
        type: Number
    })
    juniorSalary: number;

    @Prop({
        type: Number
    })
    middleSalary: number;

    @Prop({
        type: Number
    })
    seniorSalary: number;
}

export class TopPageAdvantage {
    @Prop({
        type: String
    })
    title: string

    @Prop({
        type: String
    })
    description: string;
}

@Schema({
    timestamps: true,
    versionKey: false,
    toJSON: { virtuals: true }

})
export class TopPage extends Document {
    @Prop({
        type: Object
    })
    firstCategory: TopLevelCategory;

    @Prop({
        type: String
    })
    secondCategory: string;

    @Prop({
        type: String
    })
    alias: string

    @Prop({
        type: String
    })
    title: string;

    @Prop({
        type: String
    })
    category: string;

    @Prop({
        type: Object
    })
    hh?: HhData;

    @Prop({
        type: TopPageAdvantage
    })
    advantages: TopPageAdvantage[];

    @Prop({
        type: String
    })
    seoText: string;

    @Prop({
        type: String
    })
    tagsTitle: string;

    @Prop({
        type: Array
    })
    tags: string[];
}

const TopPageSchema = SchemaFactory.createForClass(TopPage);

TopPageSchema.index({ '$**': 'text' });

TopPageSchema.virtual('id').get(function (this: TopPage) {
    return this._id;
});

export { TopPageSchema }