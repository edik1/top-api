import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';


export type ProductCharacteristic = {
    name: string
    value: string

}

@Schema({
    timestamps: true,
    versionKey: false,
    toJSON: {
        virtuals: true,
    },
})
export class Product extends Document {

    @Prop({
        type: String,
        required: true,
    })
    title: string;

    @Prop({
        type: Object,
        default: null,
    })
    image: {
        originalUrl: String
    } | null;

    @Prop({
        type: Number
    })
    price: number;

    @Prop({
        type: Number
    })
    oldPrice?: number;

    @Prop({
        type: Number
    })
    credit: number;

    @Prop({
        type: String
    })
    description: string;

    @Prop({
        type: String
    })
    advantages: string;

    @Prop({
        type: String
    })
    disAdvantages: string;

    @Prop({
        type: () => [String]
    })
    categories: string[];

    @Prop({
        type: () => [String]
    })
    tags: string[];

    @Prop({
        type: Object
    })
    characteristics: ProductCharacteristic[];
}

const ProductSchema = SchemaFactory.createForClass(Product);

ProductSchema.virtual('id').get(function (this: Product) {
    return this._id;
});

export { ProductSchema };