import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
import { Product } from 'src/product/product.schema';

@Schema({
    timestamps: true,
    versionKey: false,
    toJSON: {
        virtuals: true,
    },
})
export class Review extends Document {
    @Prop({
        type: String,
    })
    name: string;

    @Prop({
        type: String
    })
    title: string;

    @Prop({
        type: String
    })
    description: string;

    @Prop({
        type: Number
    })
    rating: number;

    @Prop({
        type: mongoose.Schema.Types.ObjectId
    })
    productId: Product | mongoose.Schema.Types.ObjectId | string | null;

}

const ReviewSchema = SchemaFactory.createForClass(Review);

ReviewSchema.virtual('id').get(function (this: Review) {
    return this._id;
});

export { ReviewSchema }