import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { disconnect } from 'mongoose';
import { AuthDto } from 'src/auth/dto/auth.dto';

const loginDto: AuthDto = {
    login: 'a2@a.ru',
    password: '1'
};

describe('AuthController (e2e)', () => {
    let app: INestApplication;
    let createdId: string;
    let httpServer;
    let token: string;

    beforeEach(async () => {
        const { body } = await request(app.getHttpServer())
            .post('/auth/login')
            .send(loginDto);
        token = body.access_token;
    });

    beforeAll(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();
        app = moduleFixture.createNestApplication();
        await app.init();
        httpServer = app.getHttpServer()
    })

    it('/api/auth/login (POST) - success', (done) => {
        request(httpServer)
            .post('/auth/login')
            // .set('Authorization', 'Bearer ' + token)
            .send(loginDto)
            .expect(200)
            .then(({ body }: request.Response) => {
                expect(body.access_token).toBeDefined();
                done();
            });

    })

    // it('/api/auth/login (POST) - fail', () => {
    //     request(httpServer)
    //         .post('/auth/login')
    //         .set('Authorization', 'Bearer ' + token + 'sdsdf')
    //         .expect(404)

    // });

    it('/api/auth/login (POST) - fail password', () => {
        request(httpServer)
            .post('/auth/login')
            // .set('Authorization', 'Bearer ' + token)
            .send({ ...loginDto, password: '2' })
            .expect(401, {
                statusCode: 401,
                message: "Неправильный пароль",
                error: "Unauthorized"
            })


    });

    it('/api/auth/login (POST) - fail login', () => {
        request(httpServer)
            .post('/auth/login')
            // .set('Authorization', 'Bearer ' + token)
            .send({ ...loginDto, login: 'aaaa@a.ru' })
            .expect(401, {
                statusCode: 401,
                message: "Пользователь с таким email не найден",
                error: "Unauthorized"
            })


    })


    afterAll(() => {
        disconnect();
    })

});